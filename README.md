# Twitter to minitel

Un programme pour vous permettre d'afficher en temps réel des tweets sur votre minitel suivant un filtre prédéfini.

## Installation

Installation de Python et pip :
```
sudo apt install python3 python3-pip
```

Installation de la librairie pour interagir avec l'API Twitter :
```
pip3 install python-twitter
```

Récupération des scripts :
```
git clone https://framagit.org/Electroalex26/twitter-to-minitel.git
cd twitter-to-minitel
chmod u+x TwitterToMinitel.bash
```

## Configuration

Rendez-vous sur [https://developer.twitter.com/](https://developer.twitter.com/). Créez une nouvelle application et récupérez les clés et tokens de votre App.
Modifiez le script __TwitterToStdout.py__ en ajoutant vos clés et tokens.

Pour fabriquer l'interface entre l'ordinateur et le minitel, regarder [cet article](http://pila.fr/wordpress/?p=361).

Vérifiez quel port série vous utilisez pour communiquer avec le minitel. S'il est différent de /dev/ttyUSB0, modifiez le script __TwitterToMinitel.bash__. Exemple, si j'utilise le port /dev/ttyACM0, je vais modifier le script comme ça :
```
stty -F /dev/ttyACM0 4800 istrip cs7 parenb -parodd brkint ignpar icrnl ixon ixany opost onlcr cread hupcl isig icanon echo echoe echok
python3 TwitterToStdout.py > /dev/ttyACM0
```

Allumez le minitel et effectuez les opérations suivantes :
* Fnct + T puis A : passe en mode moniteur série
* Fnct + T puis E : désactive l'echo local
* Fnct + P puis 4 : passe la communication à 4800bauds

## Execution du programme

Ouvrez un terminal et rendez vous dans le dossier __twitter-to-minitel__ et executez la commande suivante :
```
./TwitterToMinitel.bash
```
Il va vous être demandé d'entrer l'expression à surveiller qui va servir de filtre. Entrez la et appuyez sur entrée. Le programme se lance.
Pour l'arrêter, appuyez sur Ctrl + C.

## Documentation supplémentaire

* [Fabriquer l'interface USB-Minitel](http://pila.fr/wordpress/?p=361)
* [Github de python-twitter](https://github.com/bear/python-twitter)
* [Documentation de python-twitter](https://python-twitter.readthedocs.io/en/latest/index.html)
* [Documentation des commandes de python-twitter](https://python-twitter.readthedocs.io/en/latest/twitter.html)
* [Documentation de l'API Twitter](https://developer.twitter.com/en/docs)