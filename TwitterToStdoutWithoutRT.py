# Import des librairies utiles au bon fonctionnement
import twitter
from unidecode import unidecode

# Entrez ici vos clés et tokens
CONSUMER_KEY = 'xxx'
CONSUMER_SECRET = 'xxx'
ACCES_TOKEN_KEY = 'xxx'
ACCES_TOKEN_SECRET = 'xxx'

# Création d'une fonction qui prend en entrée un string et un entier.
# Cette fonction retourne un tableau de strings de taille maximale la longueur définie plus haut
def cuttingString(texte, longueur):
	export = []
	i = 0
	while len(texte) - i*longueur > longueur:
		export.append(texte[i*longueur:(i+1)*longueur])
		i += 1
	export.append(texte[i*longueur:len(texte)])
	return export

# Création de l'accès à l'API
api = twitter.Api(consumer_key = CONSUMER_KEY, consumer_secret = CONSUMER_SECRET, access_token_key = ACCES_TOKEN_KEY, access_token_secret = ACCES_TOKEN_SECRET)
# Création du stream à écouter avec le filtre défini par l'utilisateur au lancement du programme
stream = api.GetStreamFilter(track=[input("Expression à surveiller ? ")])

# Boucle infinie
while True:
	# Récupère le dernier tweet correspondant au filtre
	tweet = next(stream)
	# Si seulement il ne s'agit pas d'un retweet ...
	if not "RT" in tweet["text"]:
		# Envoi le caractère 0x07 au minitel qui a pour but de faire sonner son buzzer
		print("\x07")
		# Prépare le tweet à s'afficher
		texte = tweet["text"]
		nom = tweet["user"]["screen_name"]
		affichage = "@" + nom + " : " + unidecode(texte)
		# L'adapte à l'écran du minitel (80 colonnes) et l'affiche
		affichagecut = cuttingString(affichage, 80)
		for i in range(len(affichagecut)):
            print(affichagecut[i])